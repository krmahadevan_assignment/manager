package com.rationaleemotions.manager.pojos;

public record Records(long from, long to) {
}
