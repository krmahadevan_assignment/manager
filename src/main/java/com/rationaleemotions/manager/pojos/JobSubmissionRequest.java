package com.rationaleemotions.manager.pojos;

public record JobSubmissionRequest(long jobId, long from, long to, long records) {
}
