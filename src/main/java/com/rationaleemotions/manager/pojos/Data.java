package com.rationaleemotions.manager.pojos;


import java.util.List;

public record Data(Records records, List<ThirdPartyTableDao> testData) {
}
