package com.rationaleemotions.manager.pojos;

public record Node(String ip, int port) {

    public String getId() {
        return ip + ":" + port;
    }

    public static Node fromId(String id) {
        String[] parts = id.split(":");
        if (parts.length != 2) {
            throw new IllegalArgumentException("Invalid node id: " + id);
        }
        return new Node(parts[0], Integer.parseInt(parts[1]));
    }
}
