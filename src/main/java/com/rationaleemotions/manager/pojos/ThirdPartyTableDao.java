package com.rationaleemotions.manager.pojos;

import com.rationaleemotions.manager.entity.ThirdPartyTable;
import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(chain = true, fluent = true)
@Getter
public class ThirdPartyTableDao {
    private final Long id;
    private final String name;
    private final int age;
    private final boolean employed;
    private final String address;
    private final String phoneNumber;

    public ThirdPartyTableDao(ThirdPartyTable table) {
        this.id = table.id();
        this.name = table.name();
        this.age = table.age();
        this.employed = table.employed();
        this.address = table.address();
        this.phoneNumber = table.phoneNumber();
    }
}
