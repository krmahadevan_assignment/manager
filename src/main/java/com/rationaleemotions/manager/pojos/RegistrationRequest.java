package com.rationaleemotions.manager.pojos;

public record RegistrationRequest(String ip, int port) {
}
