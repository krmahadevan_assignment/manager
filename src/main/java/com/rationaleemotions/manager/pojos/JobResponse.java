package com.rationaleemotions.manager.pojos;

public record JobResponse(long jobId, String reason, boolean isError) {

    public JobResponse(long jobId, String reason) {
        this(jobId, reason, false);
    }
}
