package com.rationaleemotions.manager.pojos;

public enum JobStatus {

    Aborted,
    Submitted,
    Processing,
    Success,
    Failed;

    public static boolean isCompleted(JobStatus jobStatus) {
        return jobStatus == Failed || jobStatus == Success;
    }

    public static boolean isInFlight(JobStatus jobStatus) {
        return jobStatus == Submitted || jobStatus == Processing;
    }
}
