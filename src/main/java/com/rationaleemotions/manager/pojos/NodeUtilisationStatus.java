package com.rationaleemotions.manager.pojos;

public record NodeUtilisationStatus(boolean busy) {
}
