package com.rationaleemotions.manager.pojos;

public record JobStatusUpdateRequest(long jobId, JobStatus status, String fileLocation) {

    @SuppressWarnings("unused") // This will be called by Spring when it deserializes our payload
    public JobStatusUpdateRequest(long jobId, JobStatus status) {
        this(jobId, status, "");
    }
}
