package com.rationaleemotions.manager.pojos;

public record NodePoolStatus(boolean empty) {
}
