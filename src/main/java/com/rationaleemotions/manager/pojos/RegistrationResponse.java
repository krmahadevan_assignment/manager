package com.rationaleemotions.manager.pojos;

public record RegistrationResponse(boolean success, String message) {
}
