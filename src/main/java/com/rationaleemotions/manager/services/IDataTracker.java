package com.rationaleemotions.manager.services;

import org.springframework.data.util.Pair;

import java.util.Optional;

public interface IDataTracker {

    Optional<Pair<Long, Long>> getCurrentRange();

    void updateRange(Long startRange, Long endRange);
}
