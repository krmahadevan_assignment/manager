package com.rationaleemotions.manager.services;

import com.rationaleemotions.manager.pojos.RegistrationRequest;

public interface IRegistration {

    boolean registerNewWorker(RegistrationRequest req);
}
