package com.rationaleemotions.manager.services;

import com.rationaleemotions.manager.pojos.JobResponse;
import com.rationaleemotions.manager.pojos.JobStatusUpdateRequest;
import org.springframework.transaction.annotation.Transactional;

public interface IJobScheduler {
    @Transactional
    JobResponse updateStatus(JobStatusUpdateRequest request);

    JobResponse schedule();
}
