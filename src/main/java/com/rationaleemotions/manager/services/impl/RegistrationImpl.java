package com.rationaleemotions.manager.services.impl;

import com.rationaleemotions.manager.internal.NodeManager;
import com.rationaleemotions.manager.pojos.RegistrationRequest;
import com.rationaleemotions.manager.services.IRegistration;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegistrationImpl implements IRegistration {

    private final NodeManager manager;

    @Override
    public boolean registerNewWorker(RegistrationRequest worker) {
        return manager.add(worker.ip(), worker.port());
    }
}
