package com.rationaleemotions.manager.services.impl;

import com.rationaleemotions.manager.entity.ThirdPartyTable;
import com.rationaleemotions.manager.pojos.Records;
import com.rationaleemotions.manager.pojos.Data;
import com.rationaleemotions.manager.pojos.ThirdPartyTableDao;
import com.rationaleemotions.manager.repository.ThirdPartyTableRepository;
import com.rationaleemotions.manager.services.IManageTestData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ManageTestDataImpl implements IManageTestData {

    private final ThirdPartyTableRepository repository;

    private static final Function<List<Long>, Pair<Long, Long>> minMaxFunction = keys -> {
        long from = keys.stream()
                .reduce(Math::min).orElse(1L);
        long to = keys.stream()
                .reduce(Math::max).orElse(1L);
        return Pair.of(from, to);
    };

    @Override
    @Transactional
    public Records generate(int howMany) {
        List<ThirdPartyTable> collected = generateAndCommit(howMany);
        List<Long> primaryKeys = collected.stream().map(ThirdPartyTable::id).toList();
        Pair<Long, Long> minMax = minMaxFunction.apply(primaryKeys);
        return new Records(minMax.getFirst(), minMax.getSecond());
    }

    @Override
    public Data getTestData() {
        log.info("Retrieving all data from database");
        return asTestData(repository.findAll());
    }

    @Override
    public Data getTestData(long from) {
        log.info("Retrieving data from database from {}", from);
        return asTestData(repository.findByIdGreaterThan(from));
    }

    private Data asTestData(List<ThirdPartyTable> found) {
        List<Long> primaryKeys = found.stream().map(ThirdPartyTable::id).toList();
        if (primaryKeys.isEmpty()) {
            return new Data(new Records(-1, -1), Collections.emptyList());
        }
        Pair<Long, Long> minMax = minMaxFunction.apply(primaryKeys);

        List<ThirdPartyTableDao> collected = found.stream()
                .map(ThirdPartyTableDao::new)
                .toList();
        return new Data(new Records(minMax.getFirst(), minMax.getSecond()), collected);
    }

    private List<ThirdPartyTable> generateAndCommit(int howMany) {
        return IntStream.rangeClosed(1, howMany)
                .mapToObj(it -> ThirdPartyTable.newInstance())
                .map(repository::save)
                .toList();
    }

}
