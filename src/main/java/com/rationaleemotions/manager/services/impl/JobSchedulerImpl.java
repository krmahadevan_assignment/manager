package com.rationaleemotions.manager.services.impl;

import com.rationaleemotions.manager.entity.JobScheduleLedger;
import com.rationaleemotions.manager.exceptions.JobScheduleFailedException;
import com.rationaleemotions.manager.internal.NodeManager;
import com.rationaleemotions.manager.pojos.*;
import com.rationaleemotions.manager.repository.JobScheduleLedgerRepository;
import com.rationaleemotions.manager.services.IDataTracker;
import com.rationaleemotions.manager.services.IJobScheduler;
import com.rationaleemotions.manager.services.IManageTestData;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClient;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class JobSchedulerImpl implements IJobScheduler {

    private final IDataTracker tracker;
    private final IManageTestData manageTestData;
    private final JobScheduleLedgerRepository repository;
    private final NodeManager manager;
    private final RestClient restClient;

    @Value("${application.markJobAsFailedIfNotUpdatedInSeconds}")
    private int markJobAsFailedIfNotUpdatedInSeconds;

    @Value("${application.jobSubmissionEndpoint}")
    private String jobSubmissionEndpoint;

    @Transactional
    @Override
    public JobResponse updateStatus(JobStatusUpdateRequest request) {
        String fileLocation = Optional.ofNullable(request.fileLocation())
                .map(String::trim)
                .orElse("");
        if (request.status() == JobStatus.Success && fileLocation.isBlank()) {
            return new JobResponse(request.jobId(), "File location mandatory for successfully completed jobs", true);
        }
        Optional<JobScheduleLedger> found = repository.findById(request.jobId());
        if (found.isEmpty()) {
            return new JobResponse(request.jobId(), "Job not found", true);
        }
        JobScheduleLedger ledger = found.get();

        if (!JobStatus.isInFlight(ledger.jobStatus())) {
            return new JobResponse(request.jobId(),
                    "Can update only Submitted (or) In-Progress Jobs", true);
        }
        //Update the ledger table.
        ledger.lastUpdateDateTime(LocalDateTime.now())
                .fileLocation(fileLocation)
                .jobStatus(request.status());

        if (JobStatus.isCompleted(request.status())) { //Let's free the node
            manager.markNodeAsFree(Node.fromId(ledger.nodeId()));
        }
        return new JobResponse(request.jobId(), "Updated status");
    }

    @Transactional
    @Override
    public JobResponse schedule() {
        Optional<Pair<Long, Long>> currentRange = tracker.getCurrentRange();
        Data testdata;
        if (currentRange.isEmpty()) {
            //First time we are going to trigger ingestion
            testdata = manageTestData.getTestData();
        } else {
            //if we are here, it means that we already have ingested data previously.
            //So retrieve the delta of records which should be ingested.
            Pair<Long, Long> range = currentRange.get();
            testdata = manageTestData.getTestData(range.getSecond());
        }
        if (testdata.testData().isEmpty()) {
            //We found no data to be ingested. So let's return back with a proper message
            return new JobResponse(0, "No data to ingest.", true);
        }
        long records = testdata.testData().size();
        Optional<Node> freeNode = manager.getFreeNode();
        if (freeNode.isEmpty()) {
            //We don't have any worker to schedule data. Mark this attempt as aborted
            JobScheduleLedger ledger = new JobScheduleLedger()
                    .reason("No workers available")
                    .startingKey(testdata.records().from())
                    .endingKey(testdata.records().to())
                    .records((int) records)
                    .jobStatus(JobStatus.Aborted);
            Long jobId = repository.save(ledger).id();
            return new JobResponse(jobId, "No free workers.", true);
        }
        Node node = freeNode.get();

        JobScheduleLedger ledger = new JobScheduleLedger()
                .startingKey(testdata.records().from())
                .endingKey(testdata.records().to())
                .nodeId(node.getId())
                .records((int) records)
                .jobStatus(JobStatus.Submitted);
        Long jobId = repository.save(ledger).id();
        log.info("Job {} scheduled on node: {}", jobId, node);

        //Notify the worker to trigger the job.

        String url = "http://" + node.ip() + ":" + node.port() + "/" + jobSubmissionEndpoint;

        ResponseEntity<Void> response = restClient.post()
                .uri(url)
                .body(new JobSubmissionRequest(jobId, testdata.records().from(), testdata.records().to(), records))
                .retrieve()
                .toBodilessEntity();
        //Throwing an exception will cause our commit to be automatically rolled back since we are
        //inside a transaction
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new JobScheduleFailedException("Job submission failed for worker " +
                    node + " with a status code as " + response.getStatusCode().value());
        }

        tracker.updateRange(testdata.records().from(), testdata.records().to());
        manager.markNodeAsBusy(node);
        return new JobResponse(jobId,
                String.format("Submitted to worker %s", node.getId()));
    }

    @PostConstruct
    void initCleanupThread() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                List<JobScheduleLedger> inProcessing = repository.findAllProcessingJobs();
                inProcessing.stream()
                        .filter(it -> isStaleJob(it))
                        .forEach(it -> {
                            log.warn("Marking stale job {} as failed", it);
                            it.jobStatus(JobStatus.Failed);
                            it.reason("Progress Info not received from node for " + markJobAsFailedIfNotUpdatedInSeconds);
                            it.lastUpdateDateTime(LocalDateTime.now());
                            repository.save(it);
                            //Job is marked as failed. Free the node from our side.
                            //If the node is busy, it still knows how to reject concurrent requests.
                            manager.markNodeAsFree(Node.fromId(it.nodeId()));
                        });
            }
        }, 1000L, markJobAsFailedIfNotUpdatedInSeconds * 1000L);
    }

    private boolean isStaleJob(JobScheduleLedger ledger) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime updatedAt = ledger.lastUpdateDateTime();
        Duration duration = Duration.between(updatedAt, now);
        return duration.getSeconds() >= markJobAsFailedIfNotUpdatedInSeconds;
    }
}
