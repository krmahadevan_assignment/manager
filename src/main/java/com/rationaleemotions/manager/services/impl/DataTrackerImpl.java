package com.rationaleemotions.manager.services.impl;

import com.rationaleemotions.manager.entity.DataTracker;
import com.rationaleemotions.manager.repository.DataTrackerRepository;
import com.rationaleemotions.manager.services.IDataTracker;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DataTrackerImpl implements IDataTracker {

    private final DataTrackerRepository repository;

    @Override
    public Optional<Pair<Long, Long>> getCurrentRange() {
        //Always search for the first record (Since we expect to have ONLY 1 record in the table)
        List<DataTracker> found = repository.findAll();
        if (!found.isEmpty()) {
            //if the record was found, then get the first record
            DataTracker rec = found.get(0);
            return Optional.of(Pair.of(rec.startingFrom(), rec.endingWith()));
        }
        return Optional.empty();
    }

    @Transactional
    @Override
    public void updateRange(Long startRange, Long endRange) {
        //Always search for the first record (Since we expect to have ONLY 1 record in the table)
        List<DataTracker> found = repository.findAll();
        DataTracker recordToPersist = new DataTracker();
        if (!found.isEmpty()) {
            //if the record was found, then update it.
            recordToPersist = found.get(0);
        }
        recordToPersist.startingFrom(startRange).endingWith(endRange);
        repository.save(recordToPersist);
    }
}
