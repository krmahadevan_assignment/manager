package com.rationaleemotions.manager.services;

import com.rationaleemotions.manager.pojos.Records;
import com.rationaleemotions.manager.pojos.Data;

public interface IManageTestData {

    Records generate(int howMany);

    Data getTestData();

    Data getTestData(long from);
}
