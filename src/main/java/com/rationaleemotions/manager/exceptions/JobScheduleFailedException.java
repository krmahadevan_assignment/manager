package com.rationaleemotions.manager.exceptions;

public class JobScheduleFailedException extends RuntimeException {

    public JobScheduleFailedException(String message) {
        super(message);
    }
}
