package com.rationaleemotions.manager.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestClient;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;

@Configuration
public class AppConfig {

    public static final String PATH_PREFIX = "/v1/api";

    public static final String ADMIN_PATH_PREFIX = "/v1/admin";

    @Value("${application.useProxyServer}")
    private boolean useProxy;

    @Value("${application.proxyServer.host}")
    private String proxyHost;

    @Value("${application.proxyServer.port}")
    private int proxyPort;

    @Bean
    public RestClient restClient() {
        RestClient.Builder builder = RestClient.builder();
        if (useProxy) {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            SocketAddress address = new InetSocketAddress(proxyHost, proxyPort);
            Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
            requestFactory.setProxy(proxy);
            builder = builder.requestFactory(requestFactory);
        }

        return builder.build();
    }
}
