package com.rationaleemotions.manager.entity;

import com.github.javafaker.Faker;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true, fluent = true)
@Entity
@Getter
@Setter
public class ThirdPartyTable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private int age;
    private boolean employed;
    private String address;
    private String phoneNumber;

    private static final Faker FAKER = new Faker();

    public static ThirdPartyTable newInstance() {
        return new ThirdPartyTable()
                .name(FAKER.name().fullName())
                .age(FAKER.number().numberBetween(1, 100))
                .employed(FAKER.bool().bool())
                .phoneNumber(FAKER.phoneNumber().phoneNumber())
                .address(FAKER.address().streetAddress(true));
    }
}
