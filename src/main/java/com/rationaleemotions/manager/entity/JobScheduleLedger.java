package com.rationaleemotions.manager.entity;

import com.rationaleemotions.manager.pojos.JobStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors(chain = true, fluent = true)
@Entity
@Getter
@Setter
@ToString
public class JobScheduleLedger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nodeId;

    private long startingKey;

    private long endingKey;

    @Enumerated(EnumType.STRING)
    private JobStatus jobStatus;

    private int records;

    private String fileLocation;

    private LocalDateTime creationDateTime = LocalDateTime.now();

    private LocalDateTime lastUpdateDateTime = LocalDateTime.now();

    private String reason;

}
