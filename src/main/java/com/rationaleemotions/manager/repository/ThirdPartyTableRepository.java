package com.rationaleemotions.manager.repository;

import com.rationaleemotions.manager.entity.ThirdPartyTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ThirdPartyTableRepository extends JpaRepository<ThirdPartyTable, Long> {
    List<ThirdPartyTable> findByIdBetween(long start, long end);

    List<ThirdPartyTable> findByIdGreaterThan(long start);
}
