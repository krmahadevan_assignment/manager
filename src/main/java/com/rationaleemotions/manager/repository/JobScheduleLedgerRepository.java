package com.rationaleemotions.manager.repository;

import com.rationaleemotions.manager.entity.JobScheduleLedger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface JobScheduleLedgerRepository extends JpaRepository<JobScheduleLedger, Long> {

    @Query("select j from JobScheduleLedger j where j.jobStatus in ( 'Submitted','Processing') ")
    List<JobScheduleLedger> findAllProcessingJobs();
}
