package com.rationaleemotions.manager.repository;

import com.rationaleemotions.manager.entity.DataTracker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataTrackerRepository extends JpaRepository<DataTracker, Long> {
}
