package com.rationaleemotions.manager.internal;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalListener;
import com.rationaleemotions.manager.pojos.Node;
import com.rationaleemotions.manager.pojos.NodeUtilisationStatus;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Slf4j
public class NodeManager {
    private Cache<Node, NodeUtilisationStatus> nodeCache;

    @Value("${application.heartbeatInSeconds}")
    private int heartbeatInSeconds;


    @PostConstruct
    private void init() {
        //Let's clean-up the node after a second past the heartbeat frequency
        int expiryInSeconds = heartbeatInSeconds + 1;
        AtomicBoolean showCleanupMessage = new AtomicBoolean(false);
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        nodeCache = Caffeine.newBuilder()
                .expireAfterAccess(Duration.ofSeconds(expiryInSeconds))
                .executor(scheduler)
                .evictionListener(getEvictionListener(showCleanupMessage))
                .build();
        scheduler.scheduleAtFixedRate(() -> {
            nodeCache.cleanUp();
            if (showCleanupMessage.get()) {
                log.info("Cache cleanup performed");
                showCleanupMessage.set(false);
            }
        }, 0, expiryInSeconds, TimeUnit.SECONDS);
    }

    private RemovalListener<Node, NodeUtilisationStatus> getEvictionListener(AtomicBoolean showCleanupMessage) {
        return (key, value, cause) -> {
            log.warn("No heartbeats received from {} for the past {} seconds. Removing it. Root Cause: {}",
                    key, heartbeatInSeconds, cause);
            showCleanupMessage.set(true);
        };
    }

    public boolean add(String ip, int port) {
        try {
            nodeCache.put(new Node(ip, port), new NodeUtilisationStatus(false));
            log.info("Added node {}:{}", ip, port);
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean isNodeRegistered(String ip, int port) {
        Node node = new Node(ip, port);
        @Nullable NodeUtilisationStatus value = nodeCache.getIfPresent(node);
        boolean result = value != null;
        log.debug("Node {} is registered? {}. The value was {}", node, result,
                Optional.ofNullable(value).map(Record::toString).orElse("null-value"));
        return result;
    }

    public Optional<Node> getFreeNode() {
        //If we had no workers registered
        if (nodeCache.asMap().isEmpty()) {
            log.info("No free node found");
            return Optional.empty();
        }
        //Find at-least one node whose status is NOT busy
        return nodeCache.asMap().entrySet().stream()
                .filter(it -> !it.getValue().busy())
                .findFirst()
                .map(it -> {
                            log.info("Found node {} for processing", it.getKey());
                            return it;
                        }
                ).map(Map.Entry::getKey);
    }

    public void markNodeAsBusy(Node node) {
        if (nodeCache.getIfPresent(node) != null) {
            nodeCache.put(node, new NodeUtilisationStatus(true));
            log.info("{} marked as busy", node);
        }
    }

    public void markNodeAsFree(Node node) {
        if (nodeCache.getIfPresent(node) != null) {
            nodeCache.put(node, new NodeUtilisationStatus(false));
            log.info("{} marked as free", node);
        }
    }
}
