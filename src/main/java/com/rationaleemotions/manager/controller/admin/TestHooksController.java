package com.rationaleemotions.manager.controller.admin;

import com.rationaleemotions.manager.configs.AppConfig;
import com.rationaleemotions.manager.internal.NodeManager;
import com.rationaleemotions.manager.pojos.NodePoolStatus;
import com.rationaleemotions.manager.pojos.Records;
import com.rationaleemotions.manager.services.IManageTestData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppConfig.ADMIN_PATH_PREFIX)
@RequiredArgsConstructor
public class TestHooksController {

    private final IManageTestData generateData;
    private final NodeManager nodeManager;

    @GetMapping("/generate/{count}")
    public ResponseEntity<Records> generate(@PathVariable int count) {
        return ResponseEntity.ok(generateData.generate(count));
    }

    @GetMapping("/node-pool-empty")
    public ResponseEntity<NodePoolStatus> nodePoolEmpty() {
        return ResponseEntity.ok(new NodePoolStatus(nodeManager.getFreeNode().isEmpty()));
    }
}
