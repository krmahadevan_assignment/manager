package com.rationaleemotions.manager.controller;

import com.rationaleemotions.manager.configs.AppConfig;
import com.rationaleemotions.manager.internal.NodeManager;
import com.rationaleemotions.manager.pojos.RegistrationRequest;
import com.rationaleemotions.manager.pojos.RegistrationResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(AppConfig.PATH_PREFIX)
@RequiredArgsConstructor
public class HeartBeatController {

    private final NodeManager manager;

    @PostMapping(
            path = "/heartbeat",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RegistrationResponse> registerHeartBeat(@RequestBody RegistrationRequest request) {
        if (manager.isNodeRegistered(request.ip(), request.port())) {
            return ok();
        }
        return badRequest(request.ip() + ":" + request.port());
    }

    private static ResponseEntity<RegistrationResponse> ok() {
        return ResponseEntity.ok(new RegistrationResponse(true, "success"));
    }

    private static ResponseEntity<RegistrationResponse> badRequest(String id) {
        return ResponseEntity.badRequest()
                .body(new RegistrationResponse(false, id + " is NOT registered"));
    }
}

