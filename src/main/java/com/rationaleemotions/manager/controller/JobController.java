package com.rationaleemotions.manager.controller;

import com.rationaleemotions.manager.configs.AppConfig;
import com.rationaleemotions.manager.pojos.JobResponse;
import com.rationaleemotions.manager.pojos.JobStatusUpdateRequest;
import com.rationaleemotions.manager.services.IJobScheduler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppConfig.PATH_PREFIX)
@RequiredArgsConstructor
public class JobController {

    private final IJobScheduler jobScheduler;

    @PostMapping(
            path = "/schedule",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<JobResponse> schedule() {
        JobResponse job = jobScheduler.schedule();
        if (job.isError()) {
            return ResponseEntity.unprocessableEntity().body(job);
        }
        return ResponseEntity.ok(job);
    }

    @PostMapping(
            path = "/job-status",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<JobResponse> updateStatus(@RequestBody JobStatusUpdateRequest request) {
        JobResponse job = jobScheduler.updateStatus(request);
        if (job.isError()) {
            return ResponseEntity.unprocessableEntity().body(job);
        }
        return ResponseEntity.ok(job);
    }
}