package com.rationaleemotions.manager.controller;

import com.rationaleemotions.manager.configs.AppConfig;
import com.rationaleemotions.manager.pojos.RegistrationRequest;
import com.rationaleemotions.manager.pojos.RegistrationResponse;
import com.rationaleemotions.manager.services.IRegistration;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppConfig.PATH_PREFIX)
@RequiredArgsConstructor
public class RegistrationController {

    private final IRegistration registrationCapabilities;

    @PostMapping(
            path = "/register",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RegistrationResponse> registerNewWorker(@RequestBody RegistrationRequest request) {
        if (registrationCapabilities.registerNewWorker(request)) {
            return ResponseEntity.ok(new RegistrationResponse(true, "Registration Successful."));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new RegistrationResponse(false, "Registration failed."));
    }


}
