# Manager service

This repo builds a very basic data ingestion orchestration server.

## Pre-requisites

* Maven
* JDK17
* Docker (builds generate a docker image as well)

## Building the code

```shell
mvn clean package -Dmaven.test.skip=true
```

## Running the server

* First download [this docker-compose file](https://gitlab.com/krmahadevan_assignment/environment/-/blob/main/docker-compose-env.yaml?ref_type=heads)
* Bring up the environment by running:

```shell
docker-compose -f docker-compose-infra.yaml up
```

* Start the server by running

```shell
mvn clean test-compile spring-boot:run
```
This command brings up an app server listening on `4444`.

Following commands are supported by this app server.

```http request
### Generate Test data (To be used by Tests ONLY)

GET http://localhost:4444/v1/admin/generate/20 HTTP/1.1

### Check if node pool is empty (To be used by Tests ONLY)

GET http://localhost:4444/v1/admin/node-pool-empty HTTP/1.1

### Register a new worker

POST http://localhost:4444/v1/api/register HTTP/1.1
Content-Type: application/json

{
  "ip": "localhost",
  "port": 9090
}

### Register Heart beats

POST http://localhost:4444/v1/api/heartbeat HTTP/1.1
Content-Type: application/json

{
  "ip": "localhost",
  "port": 1234
}

### Schedule a job

POST http://localhost:4444/v1/api/schedule HTTP/1.1


### Update a job

POST http://localhost:4444/v1/api/job-status
Content-Type: application/json

{
  "jobId": 14,
  "status": "Success",
  "fileLocation": "/Users/home"
}
```

